import React, {Component} from "react";
import {Navigate} from "react-router-dom";
import '../css/App.css';
import Day from './Day'

const url = 'https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/';

function Loader() {
    let preloader = document.getElementById("preload");
    if (preloader !== null) {
        setTimeout(() => {
            preloader.style.opacity = 1;
            let interpreloader = setInterval(() => {
                preloader.style.opacity = preloader.style.opacity - 0.05;
                if (preloader.style.opacity <=0.05){
                    clearInterval(interpreloader);
                    preloader.style.display = "none";
                }
            },15);
        }, 100);
    }
    return (<></>);
}

class Weather extends Component {

    constructor(props) {
        super(props);
        this.state = {
            forecastGot: false,
            days: [],
            city: 'Kyiv',
            latitude: 50.5289015,
            longitude: 30.6113996,
            woeid: 924938,
            useDefaultLocation: true
        }
        this.getUserLocation = this.getUserLocation.bind(this);
        this.helpGetForecast = this.helpGetForecast.bind(this);
        this.getForecast = this.getForecast.bind(this);
        this.getPicture = this.getPicture.bind(this);
    }

    getUserLocation() {
        console.log('Getting user location..');
        navigator.geolocation.getCurrentPosition((location) => {
            this.setState({
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                useDefaultLocation: false
            });
            console.log(location.coords.latitude + ', ' + location.coords.longitude);
            this.getForecast();
        }, () => {
            this.getForecast();
        });
    }

    helpGetForecast() {
        const url0 = url + `${this.state.woeid}`;
        fetch(url0)
            .then((res) => res.json())
            .then((json) => {
                const weather = json.consolidated_weather;
                console.log(weather);
                for (let i = 0; i < 6; i += 1) {
                    this.state.days.push(weather[i]);
                }
                this.setState({
                    forecastGot: true
                });
            });
    }

    getForecast() {
        console.log("Getting forecast..");
        if (this.state.useDefaultLocation) {
            this.helpGetForecast();
            console.log('Use default location');
        }
        else {
            console.log('Not use default location');
            const url0 = url + `search/?lattlong=${this.state.latitude},${this.state.longitude}`;
            fetch(url0)
                .then((res) => res.json())
                .then((cities) => {
                    const city = cities[0];
                    const latitudeAndLongitude = city.latt_long.split(',');
                    this.setState({
                        city: city.title,
                        latitude: Number.parseFloat(latitudeAndLongitude[0]),
                        longitude: Number.parseFloat(latitudeAndLongitude[1]),
                        woeid: city.woeid
                    });
                }).then(this.helpGetForecast);
        }
    }

    getPicture(wStateAbbr) {
        return `https://www.metaweather.com/static/img/weather/png/64/${wStateAbbr}.png`
    }

    componentDidMount() {
        if (this.props.isLoggedIn) this.getUserLocation();
    }

    render() {
        return (
            <div className='Weather-background'>
                {!this.props.isLoggedIn ? <Navigate to="/login" replace={true}/> : null}
                <h2 className="Weather-app">Weather App</h2>
                {this.state.forecastGot ? <h3 className="Weather-city">{this.state.city}</h3> : null}
                {!this.state.forecastGot ? <div id="preload"/> : null}
                {!this.state.forecastGot ? <Loader/> :
                
                <div id="container">
                    {this.state.days.map((day) =>
                    <div className="weather-card">                        
                        <Day key={day.id.toString()} date={day.applicable_date}
                             weatherPicture={this.getPicture(day.weather_state_abbr)}
                             weatherState={day.weather_state_name}
                             maxTemp={Math.round(day.max_temp)} minTemp={Math.round(day.min_temp)}
                             windDirection={Math.round(day.wind_direction)}
                             windSpeed={Math.round(day.wind_speed)} humidity={day.humidity}
                             visibility={(day.visibility).toFixed(1)}
                             pressure={day.air_pressure} confidence={day.predictability}/>
                    </div>)}
                </div> }
            </div>
        )
    }
}

export default Weather;
