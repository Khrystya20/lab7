import React, {Component} from 'react';
import {Routes, Route} from 'react-router-dom';
import Weather from './Weather'
import Login from './Login'
import '../css/App.css';

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn : false
        }
        this.setLoggedIn = this.setLoggedIn.bind(this);
    }

    setLoggedIn(isLoggedIn) {
        this.setState({
            isLoggedIn: isLoggedIn
        });
    }

    render() {
        return (
            <Routes>
                <Route path='login' element={<Login isLoggedIn={this.state.isLoggedIn} setLoggedIn={this.setLoggedIn}/>}/>
                <Route path='dashboard' element={<Weather isLoggedIn={this.state.isLoggedIn} setLoggedIn={this.setLoggedIn}/>}/>
                <Route path='/' element={<Login isLoggedIn={this.state.isLoggedIn} setLoggedIn={this.setLoggedIn}/>}/>
            </Routes>
        );
    }
}

export default Dashboard;